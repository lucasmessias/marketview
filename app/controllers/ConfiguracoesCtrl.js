app.controller('ConfiguracoesCtrl', function ($scope, $rootScope, $http) {
    $scope.menus = [
        {
            name: 'Home',
            link: 'home'
        },
        {
            name: 'Lojas',
            link: 'lojas'
        },
        {
            name: 'Produtos',
            link: 'produtos'
        },
        {
            name: 'Configurações',
            link: 'configuracoes'
        },

    ];
});