app.controller('HomeCtrl', function ($scope, $rootScope, $http) {
    $scope.nome = 'Mundo!'

    $scope.menus = [
        {
            name: 'Home',
            link: 'home'
        },
        {
            name: 'Lojas',
            link: 'lojas'
        },
        {
            name: 'Produtos',
            link: 'produtos'
        },
        {
            name: 'Configurações',
            link: 'configuracoes'
        }
    ];


});