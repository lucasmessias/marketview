app.controller('LojasCtrl', function ($scope, $rootScope, $http) {
    $scope.menus = [
        {
            name: 'Home',
            link: 'home'
        },
        {
            name: 'Lojas',
            link: 'lojas'
        },
        {
            name: 'Produtos',
            link: 'produtos'
        },
        {
            name: 'Configurações',
            link: 'configuracoes'
        },

    ];

    $scope.lojas = [
        {
            nome: 'Loja1'
        },
        {
            nome: 'Loja2'
        },
        {
            nome: 'Loja3'
        },
        {
            nome: 'Loja4'
        },
        {
            nome: 'Loja5'
        },
        {
            nome: 'Loja6'
        },
        {
            nome: 'Loja7'
        }
    ];
});